# tech-wiki

A backup copy of my Tiddly Wiki, normally available via https://milohax.net and hosted on keybase at https://sinewalker.keybase.pub/net/milohax/index.html

A [TiddlyWiki](https://tiddlywiki.com) is a wiki-in-one-file driven by embedded CSS and JS. Full documentation is avialable at [the TiddlyWiki home page](https://tiddlywiki.com/).

The TiddlyWiki system inserts CRLF pairs, which Git will warn you about and refuse to work with. You should turn _off_ the Git CRLF feature on your local repo:

```
git config --local core.autocrlf input
git config --local core.safecrlf false
```

This Project builds a GitLab Pages backup, available online at https://milohax-net.gitlab.io/web/tech-wiki/

Alternatively, clone the repo and load `index.html` offline. Everything is self-contained, it could be stored on a USB memory.